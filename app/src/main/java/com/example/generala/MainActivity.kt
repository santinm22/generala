package com.example.generala

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random

val frecuencia: HashMap<Int, Int> = HashMap()

class MainActivity : AppCompatActivity() {
    lateinit var texto: TextView
    lateinit var buton: Button
    lateinit var imagen1: ImageView
    lateinit var imagen2: ImageView
    lateinit var imagen3: ImageView
    lateinit var imagen4: ImageView
    lateinit var imagen5: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buton = findViewById(R.id.button)
        imagen1 = findViewById(R.id.dado1)
        imagen2 = findViewById(R.id.dado2)
        imagen3 = findViewById(R.id.dado3)
        imagen4 = findViewById(R.id.dado4)
        imagen5 = findViewById(R.id.dado5)
        texto = findViewById(R.id.textoResultado)
        buton.setOnClickListener(){
            val dados = List(5) { Random.nextInt(1, 7) }
            generar(dados)
            println(frecuencia)
            actualizarImagen(dados,imagen1, imagen2, imagen3, imagen4, imagen5)
            Toast.makeText(this, "Presiono el boton", Toast.LENGTH_SHORT).show()
            texto.text = resultado()
            println(dados)
        }
    }
}
fun generar(dados: List<Int>){
        frecuencia.clear()
        for (i in dados) {
            var con = frecuencia[i]
            if (con == null) con = 0
            frecuencia[i] = con + 1
        }
}

fun actualizarImagen(dados:List<Int>, imagen1: ImageView, imagen2: ImageView, imagen3: ImageView, imagen4: ImageView, imagen5: ImageView){
    var counter = 0
    for (i in dados) {
        counter++
        if (i == 1) {
            if (counter == 1) {
                imagen1.setImageResource(R.drawable.dice_1)
            } else if (counter == 2) {
                imagen2.setImageResource(R.drawable.dice_1)
            } else if (counter == 3) {
                imagen3.setImageResource(R.drawable.dice_1)
            } else if (counter == 4) {
                imagen4.setImageResource(R.drawable.dice_1)
            } else if (counter == 5) {
                imagen5.setImageResource(R.drawable.dice_1)
            }
        } else if (i == 2) {
            if (counter == 1) {
                imagen1.setImageResource(R.drawable.dice_2)
            } else if (counter == 2) {
                imagen2.setImageResource(R.drawable.dice_2)
            } else if (counter == 3) {
                imagen3.setImageResource(R.drawable.dice_2)
            } else if (counter == 4) {
                imagen4.setImageResource(R.drawable.dice_2)
            } else if (counter == 5) {
                imagen5.setImageResource(R.drawable.dice_2)
            }
        } else if (i == 3) {
            if (counter == 1) {
                imagen1.setImageResource(R.drawable.dice_1)
            } else if (counter == 2) {
                imagen2.setImageResource(R.drawable.dice_3)
            } else if (counter == 3) {
                imagen3.setImageResource(R.drawable.dice_3)
            } else if (counter == 4) {
                imagen4.setImageResource(R.drawable.dice_3)
            } else if (counter == 5) {
                imagen5.setImageResource(R.drawable.dice_3)
            }
        } else if (i == 4) {
            if (counter == 1) {
                imagen1.setImageResource(R.drawable.dice_4)
            } else if (counter == 2) {
                imagen2.setImageResource(R.drawable.dice_4)
            } else if (counter == 3) {
                imagen3.setImageResource(R.drawable.dice_4)
            } else if (counter == 4) {
                imagen4.setImageResource(R.drawable.dice_4)
            } else if (counter == 5) {
                imagen5.setImageResource(R.drawable.dice_4)
            }
        } else if (i == 5) {
            if (counter == 1) {
                imagen1.setImageResource(R.drawable.dice_5)
            } else if (counter == 2) {
                imagen2.setImageResource(R.drawable.dice_5)
            } else if (counter == 3) {
                imagen3.setImageResource(R.drawable.dice_5)
            } else if (counter == 4) {
                imagen4.setImageResource(R.drawable.dice_5)
            } else if (counter == 5) {
                imagen5.setImageResource(R.drawable.dice_5)
            }
        } else if (i == 6) {
            if (counter == 1) {
                imagen1.setImageResource(R.drawable.dice_6)
            } else if (counter == 2) {
                imagen2.setImageResource(R.drawable.dice_6)
            } else if (counter == 3) {
                imagen3.setImageResource(R.drawable.dice_6)
            } else if (counter == 4) {
                imagen4.setImageResource(R.drawable.dice_6)
            } else if (counter == 5) {
                imagen5.setImageResource(R.drawable.dice_6)
            }
        }
    }
}
fun resultado(): String{
    val impresion: String
    if (frecuencia.containsKey(1) && frecuencia.containsKey(2) && frecuencia.containsKey(3) && frecuencia.containsKey(4) && frecuencia.containsKey(5) || frecuencia.containsKey(2) && frecuencia.containsKey(3) && frecuencia.containsKey(4) && frecuencia.containsKey(5) && frecuencia.containsKey(6) || frecuencia.containsKey(3) && frecuencia.containsKey(4) && frecuencia.containsKey(5) && frecuencia.containsKey(6) && frecuencia.containsKey(1)){
        impresion=("Obtuviste una Escalera")
    }
    else if (frecuencia.containsValue(5)){
        impresion=("Obtuviste una Generala")
    }else if (frecuencia.containsValue(4)) {
        impresion="Obtuviste un Poker"
    }else if (frecuencia.containsValue(3) && frecuencia.containsValue(2)) {
        impresion="Obtuviste un Full"
    }else{
        impresion="No obtuviste nada"
    }
    return impresion
}



